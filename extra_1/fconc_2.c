#include<stdio.h>
#include<unistd.h>
#include<string.h>  /* strcmp */

#include<stdlib.h> /*exit */
#include<errno.h>
#include<fcntl.h>
#ifndef BUFSIZE
#define BUFSIZE 64  /*allow gcc to overide size */
#endif

/* 8eloume na ekteleistai sth morfh 
./fconc A B C or ./fconc A B 
C proairetiko arxeio eksodou
Vazoyme ta periexomena twn A,B sto C arxeio
Ara sth pio aplh periptwsh xreizomaste 3 arguments eisodoy gia na leitoyrgei
*/

ssize_t  my_write(int fd, const void * buf,size_t size);

int main(int argc, char *argv[])
{
//error message in input arguments
	
	
	char *out_f;
	out_f=argv[argc-1];
	
	int i,fd[argc-1];

	// file descriptors for i1,i2,...,in input arguments
	for(i=0;i<argc-1;i++)
		fd[i]=0;

	
	mode_t opmod=0666; // rw-rw-rw permissions
	ssize_t numRd;
	char buffer[BUFSIZE];
		
	/*idea 
	   ./fconc | x1 |x2 |x3 |out
	argc= 1    | 2  | 3 | 4 | 5    array argv[argc-1] 
       argv=  [0]  |[1] |[2]|[3]|[4]	
	*/
	for(i=0;i<argc-2;i++){ // argv[argc-1] is outputfile need other permissions in open
		fd[i]=open(argv[i+1] ,O_RDONLY);
		if (fd[i] == -1) {  //error handling fd[i] 	
		printf("error opening  file %d \n",i+1);
		exit(-1);
	  }
        }
	
	
	fd[argc-2]=open(out_f,O_CREAT| O_WRONLY ,opmod); //| O_APPEND
	if (fd[argc-2] == -1){   //error handling fd[argc-1]      
                printf("error opening output file  \n");
                exit(-1);
 	}
	
	//       int open(const char *pathname, int flags);
	/* read by owner : S_IRUSR = 00400
	   write by owner: S_IWUSR = 00200 
	   read by group : S_IRGRP = 00040
	   read by other : S_IROTH = 00004
	   write by group: S_IWGRP = 00020  
	   write by other: S_IWOTH = 00002
 	*/ 
/* 
       O_APPEND
              The file is opened in append mode.  Before each write(2), the
              file offset is positioned at the end of the file, as if with
              lseek(2).  The modification of the file offset and the write
              operation are performed as a single atomic step. */

	
	
/* create a read / write operation */ 
    int j=0;	
    while (j<argc-2){
	// read system call template 
	// ssize_t read(int fildes, void *buf, size_t nbytes);
	//if (fd[j]==0) break;
	while((numRd = read(fd[j],buffer,BUFSIZE))>0 && ( errno !=EFAULT ) ) {// '\O' indicates EOF
		//write to output which is the last in the input sequence
		my_write(fd[argc-2],buffer,(size_t) numRd);
	}
	j=j+1;
      }
	if (errno==EFAULT){
		perror("buffer is outside your accessible address space.");
		exit(-1);
	}
		
	// check for closing fd errors
	for(i=0;i<argc-1;i++){
		if (close(fd[i])==-1){
			perror("close error");
			exit(-1);
		}
	}
		
    	
	return 0;
   
}

//  ssize_t write(int fd, const void *buf, size_t nbytes);

ssize_t  my_write(int fd, const void * buf,size_t size){
	ssize_t written;
	
	while (size>0){ // something to write 
			// at least once write even if it is an error retry
	do {
		written=write(fd,buf,size);
	}while( (written<0) && (errno=EINTR ||errno==EAGAIN )); //EAGAIN = Resource temporarily unavailable 
							   // EINTR = Interuppterd function call
	if(written < 0){
	 return written ; // exit 
	}
	size = size - written ; // bytes left to be written until size=0  
	buf  = buf + written ; // pointer to right offset for next write 
	return 0 ; // return something to finish 
      }
}

