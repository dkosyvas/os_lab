#include<stdio.h>
#include<unistd.h>
#include<string.h>  /* strcmp */

#include<stdlib.h> /*exit */
#include<errno.h>
#include<fcntl.h>
#ifndef BUFSIZE
#define BUFSIZE 1024  /*allow gcc to overide size */
#endif

/* 8eloume na ekteleistai sth morfh 
./fconc A B C or ./fconc A B 
C proairetiko arxeio eksodou
Vazoyme ta periexomena twn A,B sto C arxeio
Ara sth pio aplh periptwsh xreizomaste 3 arguments eisodoy gia na leitoyrgei
*/

ssize_t  my_write(int fd, const void * buf,size_t size);

int main(int argc, char *argv[])
{
//error message in input arguments
	
	if(argc!=3 && argc!=4){
		printf("Usage: ./fconc infile1 infile2 [outfile (default:fconc.out \n");
		exit(0);
	}
	char *out_f="fconc.out";
	if(argc==4){
		out_f=argv[3];
		if(strcmp(argv[3],argv[2])==0 || strcmp(argv[3],argv[1])==0){  // ./fconc A B A 
									      // ./fconc  A B B 
										
			printf("Error input file is same as output file \n");
			printf("Give other output name \n");
			exit(0);

		}
	}
	/*File descriptors fd1 => argv[1]
			   fd2 => argv[2]
			   fd3 => argv[3](out_f)
	*/
	int fd1,fd2,fd3=0;
	// OPEN()
	
	mode_t opmod=0666;
	ssize_t numRd;
	char buffer[BUFSIZE];
	fd1=open(argv[1],O_RDONLY);
	if (fd1 == -1) {  //error handling fd1 	
		perror("open[infile1]");
		exit(-1);
	}
	
	fd2=open(argv[2],O_RDONLY);
	if(fd2 == -1){ //error handling fd2
		close(fd1); // no point to use 
		perror("open[infile2]");
		exit(-1);
	}
 
	//       int open(const char *pathname, int flags);
	/* read by owner : S_IRUSR = 00400
	   write by owner: S_IWUSR = 00200 
	   read by group : S_IRGRP = 00040
	   read by other : S_IROTH = 00004
	   write by group: S_IWGRP = 00020  
	   write by other: S_IWOTH = 00002
 	*/ 
/* 
       O_APPEND
              The file is opened in append mode.  Before each write(2), the
              file offset is positioned at the end of the file, as if with
              lseek(2).  The modification of the file offset and the write
              operation are performed as a single atomic step. */

	
	fd3=open(out_f,O_CREAT | O_WRONLY | O_APPEND ,opmod);
	if (fd3==-1){
		close(fd1);
		close(fd2);
		perror("open[outfile]");
		exit(-1);
	}
/* create a read / write operation */ 

	
	// read system call template 
	// ssize_t read(int fildes, void *buf, size_t nbytes);

	while((numRd = read(fd1,buffer,BUFSIZE))>0) {// '\O' indicates EOF
		//write to output file which opened with fd3
		my_write(fd3,buffer,(size_t) numRd);
	}
	// for argv[2]
	while((numRd=read(fd2,buffer,BUFSIZE))>0){ // '\O' indicates EOF
                //write to output file which opened with fd3
                my_write(fd3,buffer,(size_t) numRd);
	}
	// check for closing fd errors
	
	if (close(fd1)==-1){
		perror("close error");
		exit(-1);
	}

	 if (close(fd2)==-1){
                perror("close error");
                exit(-1);
        }
	
	 if (close(fd3)==-1){
                perror("close error");
                exit(-1);
        }
	
	return 0;
}


//  ssize_t write(int fd, const void *buf, size_t nbytes);

ssize_t  my_write(int fd, const void * buf,size_t size){
	ssize_t written;
	
	while (size>0){ // something to write 
			// at least once write even if it is an error retry
	do {
		written=write(fd,buf,size);
	}while( (written<0) && (errno=EINTR ||errno==EAGAIN )); //EAGAIN = Resource temporarily unavailable 
							   // EINTR = Interuppterd function call
	if(written < 0){
	 return written ; // exit 
	}
	size = size - written ; // bytes left to be written until size=0+   
	buf  = buf + written ; // pointer to right offset for next write 
	return 0 ; // return something to finish 
}
}
